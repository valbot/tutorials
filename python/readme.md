# Sommario

* **01-base:**: Raccolta tutorial di livello base. Ottimo spunto per iniziare a prendere confidenza con gli strumenti e i concetti chiave 
* **02-analisi-quantitative-finanza** raccolta di notebooks per avvicinarsi all'analisi quantitativa e allo studio degli indicatori finanziari
* **03-crittografia** raccolta di notebooks per avvicinarsi ai concetti chiave, alla base degli algoritmi crittografici. 
