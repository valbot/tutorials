#%%
import talib
from zipline.api import order_target, record, symbol, order_target_percent

'''   
This example algorithm uses the Relative Strength Index indicator as a buy/sell signal.
When the RSI is over 70, a stock can be seen as overbought and it's time to sell.
When the RSI is below 30, a stock can be seen as oversold and it's time to buy.
'''

# Setup our variables
def initialize(context):
    
    # what stock to trade - FAANG in this example
    # stocklist = ['FB', 'AMZN', 'AAPL', 'NFLX', 'GOOG']
    stocklist = ['FB', 'AMZN', 'AAPL', 'NFLX', 'GOOGL']

    # make a list of symbols for the list of tickers
    context.stocks = [symbol(s) for s in stocklist]
    
    # create equal weights of each stock to hold in our portfolio
    context.target_pct_per_stock = 1.0 / len(context.stocks)
    
    # create initial RSI threshold values for low (oversold and buy signal) and high (overbought and sell signal)
    context.LOW_RSI = 45
    context.HIGH_RSI = 70
    context.LOW_ADX = 18
    context.HIGH_ADX = 40

# Rebalance daily.
def handle_data(context, data):
    
    # Load historical pricing data for the stocks, using daily frequncy and a rolling 20 days
    closeP = data.history(context.stocks, 'close', bar_count=30, frequency="1d")
    openP = data.history(context.stocks, 'open', bar_count=30, frequency="1d")
    highP = data.history(context.stocks, 'high', bar_count=30, frequency="1d")
    lowP = data.history(context.stocks, 'low', bar_count=30, frequency="1d")
    volume = data.history(context.stocks, 'volume', bar_count=30, frequency="1d")

    ma21 = data.history(context.stocks, 'close', bar_count=21, frequency="1d").mean()
    ma50 = data.history(context.stocks, 'close', bar_count=50, frequency="1d").mean()
    ma200 = data.history(context.stocks, 'close', bar_count=200, frequency="1d").mean()
    
    rsis = {}
    adxs = {}
    
    # Loop through our list of stocks
    for stock in context.stocks:
        # Get the rsi of this stock.
        rsi = talib.RSI(closeP[stock], timeperiod=14)[-1]
        adx = talib.ADX(highP[stock], lowP[stock], closeP[stock])[-1]
        
        """
        print('RSI')
        print(rsi)
        print('ADX')
        print(adx)
        """
             
        rsis[stock] = rsi
        adxs[stock] = adx
        
        current_position = context.portfolio.positions[stock].amount
        
        #SELL
        if rsi > context.HIGH_RSI and adx > context.HIGH_ADX and current_position > 0 and data.can_trade(stock):
            order_target(stock, 0)
   
        #BUY
        elif rsi < context.LOW_RSI and adx < context.LOW_ADX and current_position == 0 and data.can_trade(stock):
            order_target_percent(stock, context.target_pct_per_stock)
