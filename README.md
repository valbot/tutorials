# Tutorials

L'obiettivo del repository è condividere il codice illustrato sul canale [Youtube](https://www.youtube.com/user/TecnologiaSuMisura).

Il repository è organizzato per linguaggi di programmazione e tecnologie. 


## Sommario degli argomenti trattati
1. Python




## Collegamenti utili

Ricordati di iscriverti al mio canale Youtube [Valerio Mellini](https://www.youtube.com/channel/UC7xlSIY5FzXj7PV_Kz3GVVQ)  

Per proporre idee o contenuti puoi contattarmi al mio profilo [LinkedIn](https://it.linkedin.com/in/valerio-mellini-0a787549) 
